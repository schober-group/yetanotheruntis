package space.schober.yetanotheruntis

import android.accounts.AccountManager
import android.content.Context
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import space.schober.yetanotheruntis.calendar.CalendarUtil

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    private lateinit var instrumentationContext: Context

    @Before
    fun setup() {
        instrumentationContext = InstrumentationRegistry.getInstrumentation().context
    }

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("space.schober.yetanotheruntis", appContext.packageName)
    }

    @Test
    fun addAccount() {
        val cur = CalendarUtil.getCursor(instrumentationContext.contentResolver, "", arrayOf())

        while (cur!!.moveToNext()) {
            val id = cur.getString(0)
            val accountName = cur.getString(1)
            val calendarDisplayName = cur.getString(2)
            val ownerAccount = cur.getString(3)
            val accountType = cur.getString(4)
            val name = cur.getString(5)
            val calSync1 = cur.getString(6)
            print(id)
        }

        AccountManager.get(instrumentationContext).accounts.forEach {
            val a = it.name
            val b = it.type
            print(a)
        }

        assertEquals(true, true)
    }
}