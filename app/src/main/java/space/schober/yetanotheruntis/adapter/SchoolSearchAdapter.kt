package space.schober.yetanotheruntis.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import androidx.fragment.app.setFragmentResult
import androidx.recyclerview.widget.RecyclerView
import space.schober.yetanotheruntis.R
import space.schober.yetanotheruntis.ui.login.LoginFragment
import space.schober.yetanotheruntis.ui.login.LoginFragment.Companion.EXTRA_SCHOOL_NAME
import space.schober.yetanotheruntis.ui.login.LoginFragment.Companion.EXTRA_SCHOOL_URL
import space.schober.yetanotheruntis.ui.login.SchoolSearchFragment
import space.schober.yetanotheruntis.ui.login.SchoolSearchFragment.Companion.REQUEST_KEY
import space.schober.yetanotheruntis.untis.data.School

//TODO(Save State)
class SchoolSearchAdapter(
    private val dataSet: List<School>?,
    private val fragment: Fragment
) :
    RecyclerView.Adapter<SchoolSearchAdapter.ViewHolder>() {

    /**
     * Displays school name and the address
     */
    class ViewHolder(
        private val view: View,
        private val fragment: Fragment
    ) :
        RecyclerView.ViewHolder(view) {
        val displayName: TextView = view.findViewById(R.id.schoolDisplayName)
        val address: TextView = view.findViewById(R.id.schoolAddress)

        fun bind(schoolName: String, schoolURL: String) {
            view.setOnClickListener {
                val bundle = bundleOf(
                    EXTRA_SCHOOL_NAME to schoolName,
                    EXTRA_SCHOOL_URL to schoolURL
                )

                fragment.setFragmentResult(REQUEST_KEY, bundle)
                fragment.parentFragmentManager.commit {
                    setCustomAnimations(
                        R.anim.slide_in,
                        R.anim.fade_out,
                        R.anim.fade_in,
                        R.anim.slide_out
                    )
                    replace<LoginFragment>(R.id.fragmentLogin)
                    setReorderingAllowed(true)
                    addToBackStack(SchoolSearchFragment.BACK_STACK_NAME)
                }
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.adapter_school_search, viewGroup, false)

        return ViewHolder(view, fragment)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if (dataSet != null) {
            viewHolder.bind(dataSet[position].loginName, dataSet[position].server)
            viewHolder.displayName.text = dataSet[position].displayName
            viewHolder.address.text = dataSet[position].address
        }
    }

    override fun getItemCount() = dataSet?.size ?: 0
}