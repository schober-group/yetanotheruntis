package space.schober.yetanotheruntis.adapter.poko

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Context
import space.schober.yetanotheruntis.untis.data.Klasse
import java.io.Serializable

class ExtendedAccount(
    account: Account,
    val klasse: Klasse? = null,
) : Serializable {
    companion object {
        enum class TYPE {
            HEADLINE,
            NORMAL_ACCOUNT
        }
    }

    var selected: Boolean = false
    var name: String = account.name
    var type: TYPE = if (klasse == null) TYPE.HEADLINE else TYPE.NORMAL_ACCOUNT

    /**
     * Searches for the linked account
     */
    fun getAccount(context: Context): Account? {
        val accountManager = AccountManager.get(context)
        return accountManager.accounts.find { it.name == this.name }
    }

    override fun toString(): String {
        return "[$type] $name"
    }

    object Comparator : kotlin.Comparator<ExtendedAccount>, Serializable {
        override fun compare(
            o1: ExtendedAccount,
            o2: ExtendedAccount
        ): Int = if (o2.type == Companion.TYPE.HEADLINE)
            1 // Headlines first!
        else
            o1.klasse!!.id.compareTo(o2.klasse!!.id)
    }
}



