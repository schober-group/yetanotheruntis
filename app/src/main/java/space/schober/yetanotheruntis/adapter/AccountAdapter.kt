@file:Suppress("unused", "unused")

package space.schober.yetanotheruntis.adapter

import android.accounts.Account
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import space.schober.yetanotheruntis.R
import space.schober.yetanotheruntis.adapter.poko.ExtendedAccount
import space.schober.yetanotheruntis.adapter.poko.ExtendedAccount.Companion.TYPE.*
import java.util.*

class AccountAdapter(_data: HashMap<String, SortedSet<ExtendedAccount>>, var view: View) :
    RecyclerView.Adapter<AccountAdapter.ViewHolder>() {

    var data = _data
        set(value) {
            notifyDataSetChanged()
            field = value
        }


    var editMode = false
    var currentlySelected: Int = 0
        set(value) {
            val fabAdd = view.findViewById<FloatingActionButton>(R.id.fabAddAccount)
            val fabRemove = view.findViewById<FloatingActionButton>(R.id.fabRemoveAccount)
            if (value == 0) {
                editMode = false
                fabAdd.show()
                fabRemove.hide()
            } else {
                fabAdd.hide()
                fabRemove.show()
            }
            field = value
        }

    /**
     * ViewHolder for account overview.
     * Holding an account for a longer period of time -> 'edit mode'
     * In 'edit mode' it is possible to remove selected accounts
     */
    inner class ViewHolder(view: View, _account: ExtendedAccount) : RecyclerView.ViewHolder(view) {
        val accountNameTV: TextView = view.findViewById(R.id.accountKlassenTV)
        private val checkMark: LottieAnimationView = view.findViewById(R.id.animationSelectAccount)
        var account = _account
            set(value) {
                AdapterHelper.manageSelection(checkMark, value.selected, account.selected)
                field = value
            }

        init {
            view.setOnLongClickListener {
                editMode = true
                toggleSelect()
                return@setOnLongClickListener true
            }

            view.setOnClickListener {
                toggleSelect()
            }
        }

        private fun toggleSelect() {
            if (editMode && account.type == NORMAL_ACCOUNT) {
                account.selected = !account.selected
                if (account.selected) currentlySelected++ else currentlySelected--

                AdapterHelper.playAnimation(checkMark, account.selected)
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.adapter_account, viewGroup, false)

        return ViewHolder(view, ExtendedAccount(Account("placeholder", "placeholder")))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val account = getAccount(position)
        viewHolder.account = account


        viewHolder.accountNameTV.apply {
            if (account.type == HEADLINE) {
                paintFlags = Paint.UNDERLINE_TEXT_FLAG
                setTextAppearance(R.style.TextAppearance_AppCompat_Display1)
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                text = account.name
            } else {
                paintFlags = 0
                setTextAppearance(R.style.TextAppearance_AppCompat_Medium)
                textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                text = account.klasse!!.name
            }
        }

    }

    override fun getItemCount(): Int {
        var sum = 0

        data.forEach {
            sum += it.value.size
        }

        return sum
    }

    /**
     * Returns account with desired position
     * @param position Which position
     */
    private fun getAccount(position: Int): ExtendedAccount {
        var endPosition = 0
        var key: String? = null
        var setPosition: Int? = null

        run breaking@{
            data.forEach {
                val sizeSet = it.value.size
                val currentPosition = endPosition
                endPosition += sizeSet

                if (endPosition > position) {
                    key = it.key
                    setPosition = position - currentPosition
                    return@breaking
                }
            }
        }

        return data[key]!!.elementAt(setPosition!!)!!
    }


    fun deselectAll() {
        for (i in 0 until itemCount) {
            getAccount(i).selected = false
            notifyItemChanged(i)
        }
        currentlySelected = 0
    }
}