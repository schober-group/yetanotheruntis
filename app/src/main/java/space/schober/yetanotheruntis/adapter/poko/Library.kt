package space.schober.yetanotheruntis.adapter.poko


import java.io.Serializable

class Library {
    companion object {
        enum class TYPE {
            HEADLINE,
            GITHUB,
            LOTTIE
        }
    }

    var name: String? = null
    var link: String? = null
    var type: TYPE = TYPE.HEADLINE

    constructor()
    constructor(_name: String, _link: String, _type: TYPE) {
        this.name = _name
        this.link = _link
        this.type = _type
    }

    override fun toString(): String {
        return "[$type] $name"
    }

    object Comparator : kotlin.Comparator<Library>, Serializable {
        override fun compare(
            o1: Library,
            o2: Library
        ): Int = if (o2.type == Companion.TYPE.HEADLINE)
            1 // Headlines first
        else
            o1.name!!.compareTo(o2.name!!)
    }
}