package space.schober.yetanotheruntis.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import space.schober.yetanotheruntis.R
import space.schober.yetanotheruntis.untis.data.Klasse


class KlassenAdapter(
    var dataSet: List<Klasse>
) :
    RecyclerView.Adapter<KlassenAdapter.ViewHolder>() {

    /**
     * Shows each class name and a LottieAnimationView to indicate if it is currently selected or not
     */
    class ViewHolder(view: View, private var dataSet: List<Klasse>) :
        RecyclerView.ViewHolder(view) {
        val tvKlassen: TextView = view.findViewById(R.id.klassenTV)
        val checkMark: LottieAnimationView = view.findViewById(R.id.animationViewKlassen)

        var pos: Int = 0
            set(value) {
                field = value
                if (dataSet[value].selected) {
                    if (checkMark.composition != null)
                        checkMark.frame =
                            checkMark.maxFrame.toInt() // Finish animation (set check mark)
                    else
                        AdapterHelper.playAnimation(
                            checkMark,
                            true
                        ) // Show animation again (when device was rotated)
                }
            }

        init {
            view.setOnClickListener {
                toggle()
                AdapterHelper.playAnimation(checkMark, dataSet[pos].selected)
            }
        }

        private fun toggle() {
            dataSet[pos].selected = !dataSet[pos].selected
        }
    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.adapter_select_klassen, viewGroup, false)

        return ViewHolder(view, dataSet)
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)

        holder.checkMark.cancelAnimation()
        holder.checkMark.frame = holder.checkMark.minFrame.toInt()
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.pos = position
        viewHolder.tvKlassen.text = dataSet[position].name
    }

    override fun getItemCount() = dataSet.size
}