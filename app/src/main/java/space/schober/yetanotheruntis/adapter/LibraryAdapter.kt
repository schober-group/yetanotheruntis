package space.schober.yetanotheruntis.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import space.schober.yetanotheruntis.R
import space.schober.yetanotheruntis.adapter.poko.Library
import timber.log.Timber
import java.util.*


class LibraryAdapter(val data: HashMap<String, SortedSet<Library>>, var view: View) :
    RecyclerView.Adapter<LibraryAdapter.ViewHolder>() {


    /**
     * Shows the name of the dependency, a link to it and an icon depending on library type.
     * Opens link on click.
     */
    inner class ViewHolder(view: View, _library: Library, private val ctx: Context) :
        RecyclerView.ViewHolder(view) {
        val nameTV: TextView = view.findViewById(R.id.libraryNameTV)
        val linkTV: TextView = view.findViewById(R.id.libraryLinkTV)
        val icon: LottieAnimationView = view.findViewById(R.id.libraryIconLAV)

        var library = _library
            set(value) {
                AdapterHelper.manageSelection(icon, desiredState = true, currentSate = true)
                field = value
            }

        init {
            view.setOnClickListener {
                if (library.type != Library.Companion.TYPE.HEADLINE) {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse(linkTV.text.toString()))
                    ctx.startActivity(browserIntent)
                }
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.adapter_library, viewGroup, false)

        return ViewHolder(view, Library(), viewGroup.context)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val newLibrary = getLibrary(position)
        Timber.v(newLibrary.toString())

        viewHolder.apply {
            library = newLibrary
            nameTV.text = library.name
            linkTV.text = library.link

            if (library.type == Library.Companion.TYPE.HEADLINE) {
                applyHeadlineStyle(viewHolder)
            } else {
                applyLibraryStyle(viewHolder)

                icon.apply {
                    when (library.type) {
                        Library.Companion.TYPE.LOTTIE -> setAnimation(R.raw.lottie)
                        Library.Companion.TYPE.GITHUB -> setAnimation(R.raw.gears)
                        else -> setAnimation(R.raw.loading)
                    }
                    playAnimation()
                }
            }
        }
    }

    private fun applyHeadlineStyle(viewHolder: ViewHolder) {
        viewHolder.apply {
            nameTV.apply {
                paintFlags = Paint.UNDERLINE_TEXT_FLAG
                setTextAppearance(R.style.TextAppearance_AppCompat_Display1)
                textAlignment = View.TEXT_ALIGNMENT_CENTER
            }

            linkTV.visibility = View.GONE
            icon.visibility = View.GONE
        }
    }

    private fun applyLibraryStyle(viewHolder: ViewHolder) {
        viewHolder.apply {
            nameTV.apply {
                paintFlags = 0
                setTextAppearance(R.style.TextAppearance_AppCompat_Medium)
                textAlignment = View.TEXT_ALIGNMENT_TEXT_START
            }

            icon.visibility = View.VISIBLE
        }
    }

    override fun getItemCount(): Int {
        var sum = 0

        data.forEach {
            sum += it.value.size
        }

        return sum
    }


    private fun getLibrary(position: Int): Library {
        var endPosition = 0
        var key: String? = null
        var setPosition: Int? = null


        run breaking@{
            data.forEach {
                val sizeSet = it.value.size
                val currentPosition = endPosition
                endPosition += sizeSet

                if (endPosition > position) {
                    key = it.key
                    setPosition = position - currentPosition
                    return@breaking
                }
            }
        }

        return data[key]!!.elementAt(setPosition!!)!!
    }

}