package space.schober.yetanotheruntis.adapter

import com.airbnb.lottie.LottieAnimationView

object AdapterHelper {
    /**
     * Starts a LottieAnimation.
     * @param checkMark Which AnimationView
     * @param selected If the animation should be played normal (to select) or reversed (to deselect)
     */
    fun playAnimation(checkMark: LottieAnimationView, selected: Boolean) {
        var speed = checkMark.speed
        if (selected) {
            if (speed < 0) speed = -speed
        } else {
            if (speed > 0) speed = -speed
        }
        checkMark.speed = speed
        checkMark.playAnimation()
    }

    /**
     * Manages the animation after the dataset has changed
     * @param checkMark Which AnimationView
     * @param desiredState If the item should be selected
     * @param currentSate If the item is currently selected
     */
    fun manageSelection(
        checkMark: LottieAnimationView,
        desiredState: Boolean,
        currentSate: Boolean
    ) {
        if (desiredState != currentSate) {
            if (checkMark.composition != null) { // If composition is available -> finish animation
                checkMark.frame =
                    if (desiredState) checkMark.maxFrame.toInt() else checkMark.minFrame.toInt() // Finish animation (set check mark)
            } else { // If not available -> start animation (again) [when rotating device]
                playAnimation(checkMark, desiredState)
            }
        }
    }
}