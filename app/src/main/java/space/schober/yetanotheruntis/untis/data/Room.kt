package space.schober.yetanotheruntis.untis.data

data class Room(
    override val id: Long,
    override val name: String,
    override val longName: String? = null,
    val active: Boolean? = null,
    val building: String? = null,
    val did: Long? = null
) : UntisData()