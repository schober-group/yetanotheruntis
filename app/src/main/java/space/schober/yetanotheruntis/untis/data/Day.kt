package space.schober.yetanotheruntis.untis.data

import com.beust.klaxon.Json

data class Day(
    @Json(name = "day")
    override val id: Long,
    val timeUnits: List<Lesson>
) : UntisID()