package space.schober.yetanotheruntis.untis.networking

import space.schober.yetanotheruntis.untis.data.*

data class UntisResponseResultGetTeacher(
    override val jsonrpc: String,
    override val id: String,
    override val result: List<Teacher>
) : UntisResponseResult()

data class UntisResponseResultGetStudents(
    override val jsonrpc: String,
    override val id: String,
    override val result: List<Student>
) : UntisResponseResult()

data class UntisResponseResultGetKlassen(
    override val jsonrpc: String,
    override val id: String,
    override val result: List<Klasse>
) : UntisResponseResult()

data class UntisResponseResultGetSubjects(
    override val jsonrpc: String,
    override val id: String,
    override val result: List<Subject>
) : UntisResponseResult()

data class UntisResponseResultGetRooms(
    override val jsonrpc: String,
    override val id: String,
    override val result: List<Room>
) : UntisResponseResult()

data class UntisResponseResultGetDepartments(
    override val jsonrpc: String,
    override val id: String,
    override val result: List<Department>
) : UntisResponseResult()

data class UntisResponseResultGetHolidays(
    override val jsonrpc: String,
    override val id: String,
    override val result: List<Holiday>
) : UntisResponseResult()

data class UntisResponseResultGetTimegrid(
    override val jsonrpc: String,
    override val id: String,
    override val result: List<Day>
) : UntisResponseResult()

data class UntisResponseGetCurrentSchoolYear(
    override val jsonrpc: String,
    override val id: String,
    override val result: SchoolYear
) : UntisResponseResult()

data class UntisResponseGetSchoolYears(
    override val jsonrpc: String,
    override val id: String,
    override val result: List<SchoolYear>
) : UntisResponseResult()

data class UntisResponseGetTimetable(
    override val jsonrpc: String,
    override val id: String,
    override val result: List<Timetable>
) : UntisResponseResult()

data class UntisResponseImportTime(
    override val jsonrpc: String,
    override val id: String,
    override val result: Long
) : UntisResponseResult()

data class UntisResponseGetPersonId(
    override val jsonrpc: String,
    override val id: String,
    override val result: Long
) : UntisResponseResult()

data class UntisResponseGetSchool(
    override val jsonrpc: String,
    override val id: String,
    override val result: SchoolList
) : UntisResponseResult()