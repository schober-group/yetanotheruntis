package space.schober.yetanotheruntis.untis.data

data class Teacher(
    override val id: Long,
    override val name: String,
    override val longName: String? = null,
    val foreName: String? = null,
    val foreColor: String? = null,
    val backColor: String? = null,
) : UntisData()