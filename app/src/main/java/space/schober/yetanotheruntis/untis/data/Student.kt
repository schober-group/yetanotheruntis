package space.schober.yetanotheruntis.untis.data

data class Student(
    override val id: Long,
    override val name: String,
    override val longName: String,
    val key: String,
    val foreName: String,
    val gender: String
) : UntisData()