package space.schober.yetanotheruntis.untis.data

import com.beust.klaxon.Json
import space.schober.yetanotheruntis.untis.networking.UntisResponseResult

data class UntisResponseResultAuth(
    override val jsonrpc: String,
    override val id: String,
    override val result: Result
) : UntisResponseResult() {
    data class Result(
        @Json(name = "sessionId")
        val sessionID: String,

        @Json(name = "personType")
        val personType: Long,

        @Json(name = "personId")
        val personID: Long,

        @Json(name = "klasseId")
        val klasseID: Long
    )
}