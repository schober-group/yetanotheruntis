package space.schober.yetanotheruntis.untis.networking

import com.beust.klaxon.Klaxon
import com.beust.klaxon.KlaxonException
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.Headers
import com.github.kittinunf.fuel.core.ResponseResultOf
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.coroutines.awaitStringResponseResult
import space.schober.yetanotheruntis.untis.exceptions.*
import timber.log.Timber
import kotlin.random.Random

object Networking {

    /**
     * Handles both POST and the response and returns the item.
     * @param method Which "method" shall be used.
     * @param f What to do with the response (error handling is already done).
     * @return The value returned by f
     */
    suspend inline fun <reified T : UntisResponseResult, U : Any> defaultRequest(
        sessionID: String?,
        url: String,
        method: String,
        f: (untisResponse: T) -> U,
    ): U? {
        return advancedRequestWithLogin(
            sessionID, url, method, EmptyParams(), f
        )
    }

    /**
     * Handles both POST and the response.
     * @param method Which "method" shall be used.
     * @param params Params to send with the POST request.
     * @param f What to do with the response (error handling is already done).
     * @return The value of the function f
     */
    suspend inline fun <reified T : UntisResponseResult, U : Any> advancedRequestWithLogin(
        sessionID: String?,
        url: String,
        method: String,
        params: Params,
        f: (untisResponse: T) -> U,
        skipValidatingLogin: Boolean = false,
        vararg headers: Pair<String, Any> = arrayOf(Headers.COOKIE to "JSESSIONID=$sessionID")
    ): U? {
        if (!skipValidatingLogin)
            validLogin(sessionID)

        return advancedRequest(url, method, params, f, *headers)
    }

    suspend inline fun <reified T : UntisResponseResult, U : Any> advancedRequest(
        url: String,
        method: String,
        params: Any,
        f: (untisResponse: T) -> U,
        vararg headers: Pair<String, Any> = arrayOf()
    ): U? {
        val id: String = genID()

        val response = makePost(
            UntisRequest(
                id,
                method,
                params
            ),
            url,
            *headers
        )

        return handleResponse(
            response,
            id,
            f
        )
    }

    /**
     * Sends a POST request to the server
     * @param untisRequest The request to sent
     * @return The result of the request
     */
    suspend fun makePost(
        untisRequest: UntisRequest,
        url: String,
        vararg headers: Pair<String, Any>
    ): ResponseResultOf<String> {
        Timber.v(untisRequest.toJson())
        return Fuel.post(url)
            .header(*headers)
            .jsonBody(untisRequest.toJson())
            .awaitStringResponseResult()
    }


    /**
     * Handles responses in a easy to use fashion
     *
     * @param response The response, that should be taken care off.
     * @param id This is used to check, if the received id equals the original one (to make mtm-attacks harder).
     * @param f The function, that is being executed if everything is alright.
     * @return The parsed object
     */
    inline fun <reified T : UntisResponse, U> handleResponse(
        response: ResponseResultOf<String>,
        id: String,
        f: (untisResponse: T) -> U
    ): U? {
        val (_, _, result) = response

        result.fold(
            { json ->
                try {
                    // Try parsing JSON and executing function
                    return f(parseKlaxon(json, id))
                } catch (e: KlaxonException) {
                    handleResponseError(json, id)
                }
            },
            { error -> // Invalid JSON/network error
                Timber.e(error.toString())
            }
        )

        return null
    }

    fun handleResponseError(
        json: String,
        id: String
    ) {
        Timber.e("Could not parse initial json!")
        try {
            // Try parsing UntisResponseError to get more details on the error
            val error = parseKlaxon<UntisResponseError>(json, id)
            when (error.error.code) {
                ErrorCodes.Null.code -> throw NoDataException()
                ErrorCodes.TooManyResults.code -> throw TooManyResults()
                ErrorCodes.DateRangeNotWithinASingleSchoolYear.code -> throw DateRangeNotWithinASingleSchoolYear()
            }
        } catch (e: KlaxonException) {
            // Invalid UntisResponseError
            Timber.v(json)
            Timber.e("Could not parse error: ${e.stackTraceToString()}")
        }
    }

    /**
     * Parses a string to an object
     * @param json String to parse
     * @param id Checks for valid id
     * @return The parsed object
     */
    inline fun <reified T : UntisResponse> parseKlaxon(json: String, id: String): T {
        val response: T? = Klaxon().parse<T>(json)
        checkKlaxon(response, id)

        return response!!
    }

    fun checkKlaxon(response: UntisResponse?, id: String) {
        Timber.v(response.toString())

        if (response?.id == id) {
            Timber.d("Successfully parsed response!")
        } else {
            Timber.i("Wrong ID!")
            throw UntisResponseWrongID()
        }
    }

    /**
     * Generates the ID to identify corresponding messages
     * @return The ID
     */
    fun genID(): String = "%x".format(Random.nextLong(0, Long.MAX_VALUE))


    /**
     * Checks for a valid login and throws an exception if it is not valid
     */
    @Throws(InvalidLogin::class)
    fun validLogin(sessionID: String?): Boolean {
        if (sessionID != null)
            return true
        else
            throw InvalidLogin()
    }
}