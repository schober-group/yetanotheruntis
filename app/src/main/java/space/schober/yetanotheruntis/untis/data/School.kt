package space.schober.yetanotheruntis.untis.data

import com.beust.klaxon.Json
import space.schober.yetanotheruntis.untis.networking.Params


data class SearchParams(
    val search: String
) : Params()

data class School(
    val server: String,

    @Json(name = "useMobileServiceUrlAndroid")
    val useMobileServiceURLAndroid: Boolean,

    val address: String,
    val displayName: String,
    val loginName: String,

    @Json(name = "schoolId")
    val schoolID: Long,

    @Json(name = "useMobileServiceUrlIos")
    val useMobileServiceURLIos: Boolean,

    @Json(name = "serverUrl")
    val serverURL: String,

    @Json(name = "mobileServiceUrl")
    val mobileServiceURL: String
)