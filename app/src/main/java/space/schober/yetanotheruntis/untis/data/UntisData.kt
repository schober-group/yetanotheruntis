package space.schober.yetanotheruntis.untis.data

abstract class UntisID {
    abstract val id: Long
}

@Suppress("unused")
abstract class UntisData : UntisID() {
    abstract override val id: Long
    abstract val name: String
    abstract val longName: String?
    val orgid: Long? = null
    val orgname: String? = null
}