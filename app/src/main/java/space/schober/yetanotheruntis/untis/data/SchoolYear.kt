package space.schober.yetanotheruntis.untis.data

data class SchoolYear(
    override val id: Long,
    val name: String,
    val startDate: Long,
    val endDate: Long
) : UntisID()
