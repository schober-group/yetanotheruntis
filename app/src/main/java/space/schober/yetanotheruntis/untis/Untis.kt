@file:Suppress("unused")

package space.schober.yetanotheruntis.untis

import space.schober.yetanotheruntis.untis.data.*
import space.schober.yetanotheruntis.untis.helper.UntisCalendar
import space.schober.yetanotheruntis.untis.networking.*

class Untis(
    URL: String,
    val school: String,
    val username: String,
    password: String
) : UntisNetworking(URL, school, username, password) {

    /**
     * Requests all teachers WIP
     */
    private suspend fun getTeachers(): List<Teacher>? {
        TODO("message=no right for getTeachers(), code=-8509")
        return defaultRequest("getTeachers") { response: UntisResponseResultGetTeacher -> return response.result }
    }

    /**
     * Requests all students
     */
    private suspend fun getStudents(): List<Student>? {
        TODO("message=no right for getStudents(), code=-8509")
        return defaultRequest("getStudents") { response: UntisResponseResultGetStudents -> return response.result }
    }

    /**
     * Requests all classes (Klassen)
     */
    suspend fun getKlassen(): List<Klasse>? {
        return defaultRequest("getKlassen") { response: UntisResponseResultGetKlassen -> return response.result }
    }

    /**
     * Requests all subjects
     */
    suspend fun getSubjects(): List<Subject>? {
        return defaultRequest("getSubjects") { response: UntisResponseResultGetSubjects -> return response.result }
    }

    /**
     * Requests all rooms
     */
    suspend fun getRooms(): List<Room>? {
        return defaultRequest("getRooms") { response: UntisResponseResultGetRooms -> return response.result }
    }

    /**
     * Requests all departments
     */
    suspend fun getDepartments(): List<Department>? {
        return defaultRequest("getDepartments") { response: UntisResponseResultGetDepartments -> return response.result }
    }

    /**
     * Requests all holidays
     */
    suspend fun getHolidays(): List<Holiday>? {
        return defaultRequest("getHolidays") { response: UntisResponseResultGetHolidays -> return response.result }
    }

    /**
     * Requests all lessons for a week
     */
    suspend fun getTimegrid(): List<Day>? {
        return defaultRequest("getTimegridUnits") { response: UntisResponseResultGetTimegrid -> return response.result }
    }

    /**
     * Requests status data (information about lesson types; period codes; color)
     */
    suspend fun getStatusData(): UntisResponseResultGetStatusData.UntisStatusData? {
        return defaultRequest("getStatusData") { response: UntisResponseResultGetStatusData -> return response.result }
    }

    /**
     * Requests current school year.
     */
    suspend fun getCurrentSchoolYear(): SchoolYear? {
        return defaultRequest("getCurrentSchoolyear") { response: UntisResponseGetCurrentSchoolYear -> return response.result }
    }

    /**
     * Requests all lessons for a week
     */
    suspend fun getSchoolyears(): List<SchoolYear>? {
        return defaultRequest("getSchoolyears") { response: UntisResponseGetSchoolYears -> return response.result }
    }

    /**
     * Requests timetable
     */
    suspend fun getTimetable(options: TimetableParams.Options): List<Timetable>? {
        return advancedRequest(
            "getTimetable",
            params = TimetableParams(options),
            { response: UntisResponseGetTimetable -> return response.result }
        )
    }

    /**
     * Requests last time, something was imported into Untis
     */
    suspend fun getLatestImportTime(): Long? {
        return defaultRequest("getLatestImportTime") { response: UntisResponseImportTime -> return response.result }
    }

    /**
     * Get id of person.
     * @param personType Type of the Person. Either UntisTypes.STUDENT or TEACHER
     * @param surname The surname
     * @param forename The forename
     * @param dateOfBirth The date of birth, defaults to 0 (= unknown)
     */
    suspend fun getPersonId(
        personType: PersonTypes,
        surname: String,
        forename: String,
        dateOfBirth: Long = 0
    ): Long? {
        return advancedRequest(
            method = "getPersonId",
            params = PersonParams(
                type = personType.getUntisType().toLong(),
                surname = surname,
                forename = forename,
                dateOfBirth = dateOfBirth
            ), { response: UntisResponseGetPersonId -> return response.result }
        )
    }

    /**
     * Get all substitutions for the given date range. WIP
     */
    private suspend fun getSubstitutions(
        startTime: Long = UntisCalendar.currentDate,
        endTime: Long = UntisCalendar.currentDate,
        departmentId: Long = 0
    ): Any? {
        TODO("message=Index 1 out of bounds for length 1, code=-8998")
        return advancedRequest(
            method = "getSubstitutions",
            params = SubstitutionParams(startTime, endTime, departmentId),
            { response: UntisResponseResult -> return response.result }
        )
    }

    /**
     * Get all class book entries for the given date range. WIP
     */
    private suspend fun getClassregEvents(
        startTime: Long = UntisCalendar.getDateXTDaysAgo(10),
        endTime: Long = UntisCalendar.currentDate
    ): Any? {
        TODO("message=Index 0 out of bounds for length 0, code=-8998")
        return advancedRequest(
            method = "getClassregEvents",
            params = ClassregEventParams(startTime, endTime),
            { response: UntisResponseResult -> return response.result }
        )
    }

    /**
     * Get exams for the given date range. WIP
     */
    suspend fun getExams(
        startTime: Long = UntisCalendar.getDateXTDaysAgo(10),
        endTime: Long = UntisCalendar.currentDate,
        examTypeId: Long = UntisTypes.STUDENT.toLong(),
    ): Any? {
        TODO("message=Index 0 out of bounds for length 0, code=-8998")
        return advancedRequest(
            method = "getExams",
            params = ExamParams(startTime, endTime, examTypeId),
            { response: UntisResponseResult -> return response.result }
        )
    }

    /**
     * Get all exam types. WIP
     */
    suspend fun getExamTypes(): Any? {
        TODO("message=no right for getExamTypes(), code=-8509")
        return defaultRequest(
            method = "getExamTypes"
        ) { response: UntisResponseResult -> return response.result }
    }
}