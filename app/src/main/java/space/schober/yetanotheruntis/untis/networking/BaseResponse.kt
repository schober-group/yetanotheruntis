package space.schober.yetanotheruntis.untis.networking

/**
 * Abstract class for a normal response from Untis
 * @property jsonrpc Should have the value "2.0"
 * @property id This id is set by the request and the response should return the same one.
 */
abstract class UntisResponse {
    abstract val jsonrpc: String
    abstract val id: String
}

/**
 * If there is an issue, the error should be able to be parsed into this class.
 * An error occurs for example, when the user is not authorized at all or has not enough permissions.
 * @property error The error received
 */
data class UntisResponseError(
    override val jsonrpc: String,
    override val id: String,
    val error: Error
) : UntisResponse() {

    /**
     * The error returned from the RPC API.
     * Specification: https://www.jsonrpc.org/specification#error_object
     * @property message Description of the error
     * @property code The error code
     */
    data class Error(
        val message: String,
        val code: Long
    )
}

/**
 * The response must include a result.
 * @property result The result field.
 */
abstract class UntisResponseResult : UntisResponse() {
    abstract val result: Any
}