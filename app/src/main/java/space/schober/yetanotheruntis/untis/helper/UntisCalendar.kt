package space.schober.yetanotheruntis.untis.helper

import android.annotation.SuppressLint
import space.schober.yetanotheruntis.untis.data.UntisDefaults
import java.text.SimpleDateFormat
import java.util.*

object UntisCalendar {
    @SuppressLint("SimpleDateFormat")
    val sdfDate = SimpleDateFormat(UntisDefaults.DATE_FORMAT)

    val currentDate: Long = encodeDate(Calendar.getInstance())

    private fun encodeDate(calendar: Calendar): Long = sdfDate.format(calendar.time).toLong()

    private fun decode(sdf: SimpleDateFormat, long: Long): Calendar =
        Calendar.getInstance().also { it.time = sdf.parse(long.toString())!! }

    fun decodeDate(long: Long): Calendar = decode(sdfDate, long)

    fun decodeTime(long: Long): Calendar = Calendar.getInstance().also {
        it.set(Calendar.HOUR_OF_DAY, (long / 100).toInt())
        it.set(Calendar.MINUTE, (long % 100).toInt())
        it.set(Calendar.SECOND, 0)
    }


    fun getDateXTDaysAgo(x: Int): Long = getDateInXTDays(-x)

    fun getDateInXTDays(x: Int): Long {
        val cal: Calendar = Calendar.getInstance().also { it.add(Calendar.DATE, x) }
        return encodeDate(cal)
    }

    fun setTimeOnDate(date: Calendar, time: Calendar): Calendar {
        val calOut: Calendar = date.clone() as Calendar
        calOut.run {
            set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY))
            set(Calendar.MINUTE, time.get(Calendar.MINUTE))
            set(Calendar.SECOND, time.get(Calendar.SECOND))
        }

        return calOut
    }
}