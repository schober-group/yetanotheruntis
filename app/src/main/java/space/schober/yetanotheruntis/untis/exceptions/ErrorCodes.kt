package space.schober.yetanotheruntis.untis.exceptions

enum class ErrorCodes(val code: Long) {
    Null(-99),
    TooManyResults(-6003),
    DateRangeNotWithinASingleSchoolYear(-8507),
}