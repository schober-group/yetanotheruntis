package space.schober.yetanotheruntis.untis.data

import space.schober.yetanotheruntis.untis.networking.Params

data class ClassregEventParams(
    val startTime: Long,
    val endTime: Long,
) : Params()