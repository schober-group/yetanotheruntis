package space.schober.yetanotheruntis.untis.data

data class Lesson(
    val name: String,
    val startTime: Long,
    val endTime: Long
)