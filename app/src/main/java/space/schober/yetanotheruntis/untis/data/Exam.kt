package space.schober.yetanotheruntis.untis.data

import space.schober.yetanotheruntis.untis.networking.Params

data class ExamParams(
    val startTime: Long,
    val endTime: Long,
    val examTypeId: Long = UntisTypes.STUDENT.toLong()
) : Params()