package space.schober.yetanotheruntis.untis.networking

import com.beust.klaxon.Klaxon
import space.schober.yetanotheruntis.untis.data.UntisDefaults


data class UntisRequest(
    val id: String,
    val method: String,
    val params: Any,
    val jsonrpc: String = UntisDefaults.JSONRPC
) {
    fun toJson() = Klaxon().toJsonString(this)
}

abstract class Params

open class EmptyParams : Params()

data class AuthParams(
    val user: String,
    val password: String,
    val client: String
) : Params()