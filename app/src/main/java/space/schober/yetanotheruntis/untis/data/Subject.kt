package space.schober.yetanotheruntis.untis.data

data class Subject(
    override val id: Long,
    override val name: String,
    override val longName: String? = null,
    val alternateName: String? = null,
    val active: Boolean? = null,
    val foreColor: String? = null,
    val backColor: String? = null
) : UntisData()