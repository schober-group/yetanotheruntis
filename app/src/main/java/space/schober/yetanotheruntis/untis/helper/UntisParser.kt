package space.schober.yetanotheruntis.untis.helper

import com.beust.klaxon.Klaxon
import space.schober.yetanotheruntis.untis.data.Klasse

object UntisParser {
    fun classesToString(classes: Array<Klasse>): String = Klaxon().toJsonString(classes)
    fun stringToClasses(string: String): List<Klasse>? = Klaxon().parseArray(string)
}