package space.schober.yetanotheruntis.untis.helper

import java.util.*

enum class UntisKey {
    URL,
    SCHOOL,
    USERNAME,
    DATE_RANGE,
    SYNCED_CLASSES;

    override fun toString(): String {
        return super.toString().lowercase(Locale.ROOT)
    }
}