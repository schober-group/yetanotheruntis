package space.schober.yetanotheruntis.untis.data

import com.beust.klaxon.Json
import space.schober.yetanotheruntis.untis.helper.UntisCalendar
import space.schober.yetanotheruntis.untis.networking.Params

data class TimetableParams(
    val options: Options
) : Params() {

    data class Options(
        val element: Element,
        val startDate: Long = UntisCalendar.currentDate,
        val endDate: Long = UntisCalendar.currentDate,
        val onlyBaseTimetable: Boolean = false,
        val showBooking: Boolean = false,
        val showInfo: Boolean = false,
        val showSubstText: Boolean = false,
        val showLsText: Boolean = false,
        val showLsNumber: Boolean = false,
        val showStudentgroup: Boolean = false,
        val klasseFields: List<String>? = UntisDefaults.DEFAULT_FIELDS,
        val roomFields: List<String>? = UntisDefaults.DEFAULT_FIELDS,
        val subjectFields: List<String>? = UntisDefaults.DEFAULT_FIELDS,
        val teacherFields: List<String>? = UntisDefaults.DEFAULT_FIELDS
    )

    data class Element(
        val id: String,
        val type: Long = UntisTypes.KLASSE.toLong(),
        val keyType: String = "id"
    )
}

abstract class TimetableBase {
    abstract val id: Long
    abstract val date: Long
    abstract val startTime: Long
    abstract val endTime: Long
    abstract val kl: List<Klasse>
    abstract val te: List<Teacher>
    abstract val su: List<Subject>
    abstract val ro: List<Room>
    abstract val txt: String?
    abstract val type: String?
}

data class Timetable(
    override val id: Long,
    override val date: Long,
    override val startTime: Long,
    override val endTime: Long,
    val code: String? = null,
    override val kl: List<Klasse>,
    override val te: List<Teacher>,
    override val su: List<Subject>,
    override val ro: List<Room>,
    @Json(name = "lstext")
    override val txt: String? = null,
    @Json(name = "activityType")
    override val type: String? = null
) : TimetableBase()


@Suppress("unused")
data class Substitution(
    override val id: Long,
    override val date: Long,
    override val startTime: Long,
    override val endTime: Long,
    override val kl: List<Klasse>,
    override val te: List<Teacher>,
    override val su: List<Subject>,
    override val ro: List<Room>,
    override val txt: String? = null,
    override val type: String
) : TimetableBase()



