package space.schober.yetanotheruntis.untis.data

data class Holiday(
    override val id: Long,
    override val name: String,
    override val longName: String,
    val startDate: Long,
    val endDate: Long
) : UntisData()