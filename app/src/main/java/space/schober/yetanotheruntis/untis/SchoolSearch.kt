package space.schober.yetanotheruntis.untis

import space.schober.yetanotheruntis.untis.data.SchoolList
import space.schober.yetanotheruntis.untis.data.SearchParams
import space.schober.yetanotheruntis.untis.networking.Networking
import space.schober.yetanotheruntis.untis.networking.UntisResponseGetSchool

object SchoolSearch {
    private const val DEFAULT_URL = "https://mobile.webuntis.com/ms/schoolquery2"

    suspend fun search(
        schoolName: String,
        url: String = DEFAULT_URL
    ): SchoolList? {
        return Networking.advancedRequest(
            url = url,
            method = "searchSchool",
            params = listOf(SearchParams(schoolName)),
            f = { response: UntisResponseGetSchool -> return response.result },
            headers = arrayOf()
        )
    }
}