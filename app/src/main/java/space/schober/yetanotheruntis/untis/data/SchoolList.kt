package space.schober.yetanotheruntis.untis.data

data class SchoolList(
    val size: Long,
    val schools: List<School>
)