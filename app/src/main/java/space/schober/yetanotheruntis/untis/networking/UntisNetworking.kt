package space.schober.yetanotheruntis.untis.networking

import com.github.kittinunf.fuel.core.FuelManager
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import space.schober.yetanotheruntis.untis.data.UntisDefaults
import space.schober.yetanotheruntis.untis.data.UntisResponseResultAuth
import space.schober.yetanotheruntis.untis.exceptions.InvalidLogin
import space.schober.yetanotheruntis.untis.networking.Networking.validLogin
import timber.log.Timber

open class UntisNetworking(
    URL: String,
    school: String,
    private val username: String,
    private val password: String
) {

    val completeURL = "https://$URL/WebUntis/jsonrpc.do?school=$school"
    private var client: String = UntisDefaults.CLIENT_NAME

    var sessionID: String? = null
        private set
    private var personTyp: Long? = null
    private var personID: Long? = null
    var klasseID: Long? = null
        private set

    private var authJob: Job

    init {
        FuelManager.instance.baseHeaders =
            mapOf("Content-Type" to "application/json; charset=UTF-8")

        authJob = GlobalScope.launch {
            auth()
        }
    }

    /**
     * Authenticates the user.
     * If credentials wrong -> throws InvalidLogin
     */

    @Throws(InvalidLogin::class)
    private suspend fun auth() {
        advancedRequest(
            "authenticate", AuthParams(
                username,
                password,
                client
            ), { untisResponse: UntisResponseResultAuth ->
                val result = untisResponse.result

                Timber.v(result.toString())
                sessionID = result.sessionID
                personTyp = result.personType
                personID = result.personID
                klasseID = result.klasseID
            }, true
        )
    }

    /**
     * Waits for authentication and checks if it is valid
     */
    suspend fun validateLogin() {
        authJob.join()
        validLogin(sessionID)
    }

    suspend inline fun <reified T : UntisResponseResult, U : Any> defaultRequest(
        method: String,
        f: (untisResponse: T) -> U
    ): U? {
        return Networking.defaultRequest(sessionID, completeURL, method, f)
    }

    suspend inline fun <reified T : UntisResponseResult, U : Any> advancedRequest(
        method: String,
        params: Params,
        f: (untisResponse: T) -> U,
        skipValidatingLogin: Boolean = false
    ): U? {
        return Networking.advancedRequestWithLogin(
            sessionID,
            completeURL,
            method,
            params,
            f,
            skipValidatingLogin
        )
    }
}