package space.schober.yetanotheruntis.untis.data

import space.schober.yetanotheruntis.untis.networking.Params

data class SubstitutionParams(
    val startTime: Long,
    val endTime: Long,
    val departmentId: Long = 0
) : Params()