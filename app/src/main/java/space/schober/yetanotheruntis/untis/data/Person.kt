@file:Suppress("unused")

package space.schober.yetanotheruntis.untis.data

import com.beust.klaxon.Json
import space.schober.yetanotheruntis.untis.networking.Params

data class PersonParams(
    val type: Long,
    @Json(name = "sn")
    val surname: String,
    @Json(name = "fn")
    val forename: String,
    @Json(name = "dob")
    val dateOfBirth: Long = 0,
) : Params()


enum class PersonTypes {
    TEACHER {
        override fun getUntisType(): Int = UntisTypes.TEACHER
    },
    STUDENT {
        override fun getUntisType(): Int = UntisTypes.STUDENT
    };

    abstract fun getUntisType(): Int
}