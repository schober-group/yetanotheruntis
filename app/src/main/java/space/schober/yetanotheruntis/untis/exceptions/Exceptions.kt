package space.schober.yetanotheruntis.untis.exceptions

class InvalidLogin : Throwable()
class UntisResponseWrongID : Throwable()
class AccountNotAdded : Throwable()
class InvalidCalendarState(s: String) : Throwable()

class NoDataException : Throwable()
class TooManyResults : Throwable()
class DateRangeNotWithinASingleSchoolYear : Throwable()