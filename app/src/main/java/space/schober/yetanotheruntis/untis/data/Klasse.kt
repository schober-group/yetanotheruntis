package space.schober.yetanotheruntis.untis.data

import android.os.Parcelable
import com.beust.klaxon.Json
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Parcelize
data class Klasse(
    override val id: Long,
    override val name: String,
    override val longName: String? = null,
    val active: Boolean? = null,
    val foreColor: String? = null,
    val backColor: String? = null,
    val did: Long? = null,
    val teacher1: Long? = null,
    @Json(ignored = true)
    var selected: Boolean = false
) : UntisData(), Parcelable, Serializable
