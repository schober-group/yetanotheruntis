@file:Suppress("unused")

package space.schober.yetanotheruntis.untis.data

object UntisDefaults {
    const val JSONRPC = "2.0"
    const val CLIENT_NAME = "YAU"

    val DEFAULT_FIELDS = listOf(
        "name", "externalkey", "longname", "id"
    )

    const val DATE_FORMAT = "yyyyMMdd"
    const val TIME_FORMAT = "Hm"
}


object UntisTypes {
    const val KLASSE = 1
    const val TEACHER = 2
    const val SUBJECT = 3
    const val ROOM = 4
    const val STUDENT = 5
}