package space.schober.yetanotheruntis.untis.data

data class Department(
    override val id: Long,
    override val name: String,
    override val longName: String,
) : UntisData()