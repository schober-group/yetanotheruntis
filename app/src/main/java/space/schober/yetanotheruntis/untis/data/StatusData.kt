package space.schober.yetanotheruntis.untis.data

import com.beust.klaxon.Json
import space.schober.yetanotheruntis.untis.networking.UntisResponseResult

data class UntisResponseResultGetStatusData(
    override val jsonrpc: String,
    override val id: String,
    override val result: UntisStatusData
) : UntisResponseResult() {

    data class UntisStatusData(
        val lstypes: List<LessonType>,
        val codes: List<Code>
    )

    data class LessonType(
        @Json(name = "js")
        val lesson: ColorCombo? = null,
        @Json(name = "oh")
        val officeHour: ColorCombo? = null,
        @Json(name = "sb")
        val standby: ColorCombo? = null,
        @Json(name = "bs")
        val breakSupervision: ColorCombo? = null,
        @Json(name = "ex")
        val examination: ColorCombo? = null
    )

    data class Code(
        val cancelled: ColorCombo? = null,
        val irregular: ColorCombo? = null
    )

    data class ColorCombo(
        val foreColor: String,
        val backColor: String
    )
}