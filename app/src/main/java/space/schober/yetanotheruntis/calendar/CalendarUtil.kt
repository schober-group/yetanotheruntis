package space.schober.yetanotheruntis.calendar

import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract.*
import androidx.annotation.ColorInt
import space.schober.yetanotheruntis.R
import space.schober.yetanotheruntis.untis.exceptions.InvalidCalendarState
import timber.log.Timber
import java.util.*

object CalendarUtil {

    private const val ACCOUNT_TYPE = "space.schober.yetanotheruntis"
    private val EVENT_PROJECTION: Array<String> = arrayOf(
        Calendars._ID,                     // 0
        Calendars.ACCOUNT_NAME,            // 1
        Calendars.CALENDAR_DISPLAY_NAME,   // 2
        Calendars.OWNER_ACCOUNT,           // 3
        Calendars.ACCOUNT_TYPE,            // 4
        Calendars.NAME,                    // 5
        Calendars.CAL_SYNC1,               // 6
    )

    // The indices for the projection array above.
    private const val PROJECTION_ID_INDEX: Int = 0
    private const val PROJECTION_ACCOUNT_NAME_INDEX: Int = 1
    private const val PROJECTION_DISPLAY_NAME_INDEX: Int = 2
    private const val PROJECTION_OWNER_ACCOUNT_INDEX: Int = 3
    private const val PROJECTION_ACCOUNT_TYPE_INDEX: Int = 4
    private const val PROJECTION_NAME_INDEX: Int = 5
    private const val PROJECTION_CAL_SYNC1_INDEX: Int = 6


    private fun addCalendar(
        context: Context,
        ownerAccount: String,
        calendarDisplayName: String,
        @ColorInt color: Int,
    ) {
        val accountName: String = ownerAccount
        val contentResolver = context.contentResolver
        val values = ContentValues().apply {
            // Required
            put(Calendars.ACCOUNT_NAME, accountName)
            put(Calendars.ACCOUNT_TYPE, ACCOUNT_TYPE)
            put(
                Calendars.NAME,
                getCalendarName(ownerAccount, calendarDisplayName)
            )
            put(Calendars.CALENDAR_DISPLAY_NAME, calendarDisplayName)
            put(Calendars.CALENDAR_COLOR, color)
            put(
                Calendars.CALENDAR_ACCESS_LEVEL,
                Calendars.CAL_ACCESS_OWNER
            )
            put(Calendars.OWNER_ACCOUNT, ownerAccount)

            // Optional
            put(Calendars.SYNC_EVENTS, 1)
            put(Calendars.CALENDAR_TIME_ZONE, TimeZone.getDefault().id)
            put(
                Calendars.ALLOWED_REMINDERS,
                listOf(
                    Reminders.METHOD_DEFAULT,
                    Reminders.METHOD_ALARM
                ).joinToString(",")
            )
            put(
                Calendars.ALLOWED_AVAILABILITY,
                listOf(
                    Events.AVAILABILITY_BUSY,
                    Events.AVAILABILITY_TENTATIVE
                ).joinToString(",")
            )
            put(
                Calendars.ALLOWED_ATTENDEE_TYPES,
                listOf(
                    Attendees.TYPE_REQUIRED,
                    Attendees.TYPE_RESOURCE
                ).joinToString(",")
            )
            put(Calendars.VISIBLE, 1)
        }

        val calUri = Calendars.CONTENT_URI.buildUpon()
            .appendQueryParameter(CALLER_IS_SYNCADAPTER, "true")
            .appendQueryParameter(Calendars.ACCOUNT_NAME, accountName)
            .appendQueryParameter(Calendars.ACCOUNT_TYPE, ACCOUNT_TYPE)
            .build()

        val url = contentResolver.insert(calUri, values)

        if (url != null) {
            //val id = getCalendarID(context, calendarName)
            Timber.v("Added calender: $accountName [$url]")
        } else {
            // On some devices, the calendar can't be inserted on the first try -> second try required
            Timber.w("Could not add calendar... Trying again")
            addCalendar(context, ownerAccount, calendarDisplayName, color)
        }
    }

    private fun getCalendarID(
        context: Context,
        calendarName: String,
        ownerAccount: String
    ): Long {

        val contentResolver = context.contentResolver

        val cur = getCursor(
            contentResolver,
            "((" +
                    "${Calendars.ACCOUNT_NAME} = ?) AND (" +
                    "${Calendars.OWNER_ACCOUNT} = ?) AND (" +
                    "${Calendars.ACCOUNT_TYPE} = ?) AND (" +
                    "${Calendars.NAME} = ?)" +
                    ")",
            arrayOf(ownerAccount, ownerAccount, ACCOUNT_TYPE, calendarName)
        )

        if (cur!!.count > 1)
            Timber.w("More than one matching calendar!")

        if (cur.moveToNext()) {
            return cur.getLong(PROJECTION_ID_INDEX)
        }

        throw InvalidCalendarState("No calendar found!")
    }

    private fun getCalendarName(ownerAccount: String, calendarDisplayName: String) =
        "$calendarDisplayName@$ownerAccount"

    /**
     * @return The calendar id
     */
    fun manageCalendar(context: Context, calendarDisplayName: String, ownerAccount: String): Long {
        val calendarName = getCalendarName(ownerAccount, calendarDisplayName)
        return try {
            getCalendarID(context, calendarName, ownerAccount)
        } catch (invalidCalendarState: InvalidCalendarState) {
            Timber.i("Calendar [$ownerAccount/$calendarDisplayName] missing! Inserting it...")
            addCalendar(
                context,
                ownerAccount,
                calendarDisplayName,
                context.getColor(R.color.calendar_default_color)
            )
            manageCalendar(context, calendarDisplayName, ownerAccount)
        }
    }

    fun getCursor(
        contentResolver: ContentResolver,
        selection: String = "",
        selectionArgs: Array<String> = arrayOf(),
        sortOrder: String? = null,
        projection: Array<String> = EVENT_PROJECTION,
        uri: Uri = Calendars.CONTENT_URI,
    ): Cursor? {
        return contentResolver.query(uri, projection, selection, selectionArgs, sortOrder)
    }

    @Suppress("unused")
    fun listAllCalendars(context: Context) {
        val cur = getCursor(context.contentResolver, "", arrayOf())

        while (cur!!.moveToNext()) {
            val calID: Long = cur.getLong(PROJECTION_ID_INDEX)
            val displayName: String = cur.getString(PROJECTION_DISPLAY_NAME_INDEX)
            val accountName: String = cur.getString(PROJECTION_ACCOUNT_NAME_INDEX)
            val ownerName: String = cur.getString(PROJECTION_OWNER_ACCOUNT_INDEX)
            val accountType: String = cur.getString(PROJECTION_ACCOUNT_TYPE_INDEX)
            val name: String? = cur.getString(PROJECTION_NAME_INDEX)
            val sync1: String? = cur.getString(PROJECTION_CAL_SYNC1_INDEX)

            Timber.d("$accountName/$accountType [$calID]: $displayName {$name} [$ownerName] $sync1")
        }

        cur.close()
    }
}