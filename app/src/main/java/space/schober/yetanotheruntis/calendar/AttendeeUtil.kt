@file:Suppress("unused", "unused")

package space.schober.yetanotheruntis.calendar

import android.content.ContentResolver
import android.content.ContentValues
import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract.Attendees
import space.schober.yetanotheruntis.untis.data.Klasse
import space.schober.yetanotheruntis.untis.data.Teacher

object AttendeeUtil {
    const val TAG: String = "AttendeeUtil"
    private val EVENT_PROJECTION: Array<String> = arrayOf(
        Attendees._ID,
        Attendees.EVENT_ID,
        Attendees.ATTENDEE_NAME,
        Attendees.ATTENDEE_EMAIL
    )

    // The indices for the projection array above.
    private const val PROJECTION_ID_INDEX: Int = 0
    private const val PROJECTION_EVENT_ID_INDEX: Int = 1
    private const val PROJECTION_ATTENDEE_NAME_INDEX: Int = 2
    private const val PROJECTION_ATTENDEE_EMAIL_INDEX: Int = 3


    private fun getCursor(
        contentResolver: ContentResolver,
        selection: String,
        selectionArgs: Array<String>
    ): Cursor? {
        return CalendarUtil.getCursor(
            contentResolver = contentResolver,
            selection = selection,
            selectionArgs = selectionArgs,
            projection = EVENT_PROJECTION,
            uri = Attendees.CONTENT_URI
        )
    }

    fun addAttendees(
        contentResolver: ContentResolver,
        eventID: Long,
        school: String,
        teachers: List<Teacher>,
        klassen: List<Klasse>
    ) {
        teachers.forEach { addAttendeeTeacher(contentResolver, eventID, school, it) }
        klassen.forEach { addAttendeeKlasse(contentResolver, eventID, school, it) }
    }

    private fun addAttendeeTeacher(
        contentResolver: ContentResolver, eventID: Long, school: String, teacher: Teacher
    ) {
        addAttendee(contentResolver, eventID, school, teacher.foreName ?: teacher.name)
    }

    private fun addAttendeeKlasse(
        contentResolver: ContentResolver, eventID: Long, school: String, klasse: Klasse
    ) {
        addAttendee(contentResolver, eventID, school, klasse.name)
    }

    private fun getName(name: String, school: String) = "$name@$school"

    private fun addAttendee(
        contentResolver: ContentResolver, eventID: Long, school: String, name: String
    ) {
        val values = ContentValues().apply {
            put(Attendees.ATTENDEE_NAME, name)
            put(Attendees.ATTENDEE_EMAIL, getName(name, school))
            put(
                Attendees.ATTENDEE_RELATIONSHIP,
                Attendees.RELATIONSHIP_PERFORMER
            )
            put(Attendees.ATTENDEE_TYPE, Attendees.TYPE_REQUIRED)
            put(
                Attendees.ATTENDEE_STATUS,
                Attendees.ATTENDEE_STATUS_ACCEPTED
            )
            put(Attendees.EVENT_ID, eventID)
        }
        addAttendeesToCalendar(contentResolver, values)
    }

    private fun addAttendeesToCalendar(
        contentResolver: ContentResolver,
        values: ContentValues
    ): Uri? {
        return contentResolver.insert(Attendees.CONTENT_URI, values)
    }

    fun getAttendees(contentResolver: ContentResolver, eventID: Long): MutableList<Attendee> {
        val cursor = getCursor(
            contentResolver,
            "(${Attendees.EVENT_ID} = ?)",
            arrayOf(eventID.toString())
        )

        val mutableList = mutableListOf<Attendee>()

        while (cursor!!.moveToNext()) {
            mutableList.add(
                Attendee(
                    cursor.getString(PROJECTION_ATTENDEE_NAME_INDEX),
                    cursor.getString(PROJECTION_ATTENDEE_EMAIL_INDEX)
                )
            )
        }

        return mutableList
    }

    data class Attendee(var name: String, var email: String)
}