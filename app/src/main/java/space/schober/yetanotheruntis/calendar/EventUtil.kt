@file:Suppress("unused", "unused")

package space.schober.yetanotheruntis.calendar

import android.content.ContentResolver
import android.content.ContentUris
import android.content.ContentValues
import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract.Events
import space.schober.yetanotheruntis.untis.data.Klasse
import space.schober.yetanotheruntis.untis.data.Teacher
import space.schober.yetanotheruntis.untis.data.Timetable
import space.schober.yetanotheruntis.untis.helper.UntisCalendar
import timber.log.Timber
import java.util.*

object EventUtil {
    const val TAG: String = "EventUtil"
    private val EVENT_PROJECTION: Array<String> = arrayOf(
        Events._ID,
        Events.CALENDAR_ID,
        Events.DTSTART,
        Events.DTEND,
        Events.TITLE,
        Events.LAST_SYNCED,
    )

    // The indices for the projection array above.
    private const val PROJECTION_ID_INDEX: Int = 0
    private const val PROJECTION_CALENDAR_ID_INDEX: Int = 1
    private const val PROJECTION_DTSTART_INDEX: Int = 2
    private const val PROJECTION_DTEND_INDEX: Int = 3
    private const val PROJECTION_TITLE_INDEX: Int = 4
    private const val PROJECTION_LAST_SYNCED_INDEX: Int = 5


    private fun getCursor(
        contentResolver: ContentResolver,
        selection: String,
        selectionArgs: Array<String>
    ): Cursor? {
        return CalendarUtil.getCursor(
            contentResolver = contentResolver,
            selection = selection,
            selectionArgs = selectionArgs,
            projection = EVENT_PROJECTION,
            uri = Events.CONTENT_URI
        )
    }

    private fun getCursorToCalendar(
        contentResolver: ContentResolver,
        calID: Long,
        additionalQuery: String = "",
        additionalParameters: Array<String> = arrayOf()
    ): Cursor? {
        return getCursor(
            contentResolver,
            "(${Events.CALENDAR_ID} = ?) $additionalQuery",
            arrayOf(calID.toString()).plus(additionalParameters)
        )
    }

    private fun getAllEventIDs(contentResolver: ContentResolver, calID: Long): List<Long> {
        val cur = getCursorToCalendar(contentResolver, calID)

        val list: MutableList<Long> = mutableListOf()

        while (cur!!.moveToNext()) {
            list.add(cur.getLong(PROJECTION_ID_INDEX))
        }

        return list
    }

    private fun deleteEvent(contentResolver: ContentResolver, eventID: Long) {
        val deleteUri: Uri =
            ContentUris.withAppendedId(Events.CONTENT_URI, eventID)
        val rows: Int = contentResolver.delete(deleteUri, null, null)
        Timber.v("Rows deleted: $rows")
    }

    fun deleteAllEvents(contentResolver: ContentResolver, calID: Long) {
        getAllEventIDs(contentResolver, calID).forEach { deleteEvent(contentResolver, it) }
    }

    /**
     * Adds values (event details) to calendar
     */
    private fun addEventToCalendar(contentResolver: ContentResolver, values: ContentValues): Uri? {
        return contentResolver.insert(Events.CONTENT_URI, values)
    }


    fun addToCalendar(
        contentResolver: ContentResolver,
        currentTimetable: Timetable,
        calendarID: Long,
        school: String
    ) {
        val cal = UntisCalendar.decodeDate(currentTimetable.date)
        val startTime = UntisCalendar.decodeTime(currentTimetable.startTime)
        val endTime = UntisCalendar.decodeTime(currentTimetable.endTime)

        val calStart = UntisCalendar.setTimeOnDate(cal, startTime)
        val dtstart = calStart.timeInMillis
        val calEnd = UntisCalendar.setTimeOnDate(cal, endTime)
        val dtend = calEnd.timeInMillis

        val teachers = currentTimetable.te
        val subjects = currentTimetable.su
        val rooms = currentTimetable.ro
        val klassen = currentTimetable.kl

        val timezone = TimeZone.getDefault()

        val title = subjects.joinToString(separator = ", ") { it.name }

        if (!isEventAlreadyExisting(
                contentResolver,
                calendarID,
                dtstart,
                dtend,
                title,
                teachers,
                klassen
            )
        ) {
            val values = ContentValues().apply {
                // calendar id
                put(Events.CALENDAR_ID, calendarID)

                // time
                put(Events.DTSTART, dtstart)
                put(Events.DTEND, dtend)
                put(Events.EVENT_TIMEZONE, timezone.displayName)

                // title = subject name
                put(
                    Events.TITLE,
                    title
                )

                // location = room
                put(
                    Events.EVENT_LOCATION,
                    rooms.joinToString(separator = ", ") { it.name }
                )
            }

            val uri = addEventToCalendar(contentResolver, values)
            val eventID: Long = uri!!.lastPathSegment!!.toLong()
            AttendeeUtil.addAttendees(contentResolver, eventID, school, teachers, klassen)
        } else {
            Timber.v("Event already exists!")
        }
    }

    private fun isEventAlreadyExisting(
        contentResolver: ContentResolver,
        calendarID: Long,
        dtstart: Long,
        dtend: Long,
        title: String,
        teachers: List<Teacher>,
        klassen: List<Klasse>,
    ): Boolean {
        val possibleEventsCursor =
            getEventIDs(contentResolver, calendarID, dtstart, dtend, title) ?: return false

        Timber.v("$dtstart until $dtend [$title]")
        while (possibleEventsCursor.moveToNext()) {
            var sameEvent = true
            val attendees = AttendeeUtil.getAttendees(
                contentResolver,
                possibleEventsCursor.getLong(PROJECTION_ID_INDEX)
            )


            attendees.forEach attendeeLoop@{ attendee ->
                teachers.forEach { teacher ->
                    if (teacher.name != attendee.name) {
                        var validClass = false
                        klassen.forEach klassenLoop@{ klasse ->
                            if (klasse.name == attendee.name) {
                                validClass = true
                                return@klassenLoop
                            }
                        }

                        if (!validClass) {
                            Timber.v("No match found: ${attendee.name}")
                            sameEvent = false
                            return@attendeeLoop
                        }
                    }
                }
            }

            if (sameEvent)
                return true
        }

        return false
    }


    private fun getEventIDs(
        contentResolver: ContentResolver,
        calendarID: Long,
        dtstart: Long,
        dtend: Long,
        title: String
    ): Cursor? {
        return getCursor(
            contentResolver,
            "((" +
                    "${Events.CALENDAR_ID} = ?) AND (" +
                    "${Events.DTSTART} = ?) AND (" +
                    "${Events.DTEND} = ?) AND (" +
                    "${Events.TITLE} = ?)" +
                    ")",
            arrayOf(calendarID.toString(), dtstart.toString(), dtend.toString(), title)
        )
    }
}