package space.schober.yetanotheruntis.keystore

import android.content.Context
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import space.schober.yetanotheruntis.R
import timber.log.Timber
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.PrivateKey
import java.security.PublicKey
import javax.crypto.Cipher

object MyKeystore {
    private val CHARSET = Charsets.ISO_8859_1

    private fun createKeyPair(alias: String) {
        val kpg: KeyPairGenerator = KeyPairGenerator.getInstance(
            KeyProperties.KEY_ALGORITHM_RSA,
            "AndroidKeyStore"
        )

        val parameterSpec: KeyGenParameterSpec = KeyGenParameterSpec.Builder(
            alias,
            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
        ).run {
            setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
            setKeySize(2048)
            build()
        }

        kpg.initialize(parameterSpec)
        val kp = kpg.generateKeyPair()

        Timber.v("New KeyPair generated")
    }

    private fun prepareKeyStore(alias: String): KeyStore {
        val ks: KeyStore = KeyStore.getInstance("AndroidKeyStore").apply {
            load(null)
        }

        if (!ks.containsAlias(alias)) {
            Timber.i("Creating new alias...")
            createKeyPair(alias)
        }

        return ks
    }

    private fun getKeys(context: Context): Pair<PrivateKey, PublicKey> {
        val alias = context.getString(R.string.keystore_alias)
        val ks = prepareKeyStore(alias)

        val privateKey = ks.getKey(alias, null) as PrivateKey
        val publicKey = ks.getCertificate(alias).publicKey

        return Pair(privateKey, publicKey)
    }

    private fun getCipher(): Cipher {
        return Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidKeyStoreBCWorkaround")
    }

    private fun getEncryptCipher(context: Context): Cipher {
        val inCipher = getCipher()
        val (_, publicKey) = getKeys(context)

        inCipher.init(Cipher.ENCRYPT_MODE, publicKey)
        return inCipher
    }

    private fun getDecryptCipher(context: Context): Cipher {
        val outCipher = getCipher()
        val (privateKey, _) = getKeys(context)

        outCipher.init(Cipher.DECRYPT_MODE, privateKey)
        return outCipher
    }


    fun encrypt(context: Context, unencryptedPassword: String): String {
        val cipher = getEncryptCipher(context)
        return String(cipher.doFinal(unencryptedPassword.toByteArray(CHARSET)), CHARSET)
    }


    fun decrypt(context: Context, encryptedPassword: String): String {
        val cipher = getDecryptCipher(context)
        return String(cipher.doFinal(encryptedPassword.toByteArray(CHARSET)), CHARSET)
    }
}