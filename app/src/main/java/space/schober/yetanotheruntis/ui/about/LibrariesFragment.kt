package space.schober.yetanotheruntis.ui.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import space.schober.yetanotheruntis.BuildConfig
import space.schober.yetanotheruntis.R
import space.schober.yetanotheruntis.adapter.LibraryAdapter
import space.schober.yetanotheruntis.adapter.poko.Library
import java.util.*
import kotlin.collections.HashMap

class LibrariesFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var manager: RecyclerView.LayoutManager
    private lateinit var myAdapter: LibraryAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_libraries, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setRecyclerView(view)
    }


    private fun setRecyclerView(view: View = requireView()) {
        manager = LinearLayoutManager(context)
        myAdapter =
            LibraryAdapter(
                prepareData(),
                view
            )

        recyclerView = view.findViewById<RecyclerView>(R.id.rvLibraries).apply {
            layoutManager = manager
            adapter = myAdapter
        }
    }


    private fun prepareData(): HashMap<String, SortedSet<Library>> {
        val map = prepareValues(
            R.array.lottieNames,
            R.array.lottieURIs,
            R.string.lottieHeadline,
            Library.Companion.TYPE.LOTTIE
        )

        prepareValues(
            R.array.dependenciesNames,
            R.array.dependenciesURIs,
            R.string.dependenciesHeadline,
            Library.Companion.TYPE.GITHUB,
            map,
        )

        return map
    }

    private fun prepareValues(
        namesID: Int,
        urisID: Int,
        keyID: Int,
        type: Library.Companion.TYPE,
        map: HashMap<String, SortedSet<Library>> = HashMap(),
    ): HashMap<String, SortedSet<Library>> {
        val names = resources.getStringArray(namesID)
        val uris = resources.getStringArray(urisID)
        val key = getString(keyID)

        if (BuildConfig.DEBUG && names.size != uris.size) {
            error("Assertion failed")
        }

        map[key] =
            sortedSetOf(Library.Comparator, Library(key, "", Library.Companion.TYPE.HEADLINE))

        for (i in names.indices) {
            map[key]!!.add(
                Library(
                    names[i],
                    uris[i],
                    type
                )
            )
        }

        return map
    }
}