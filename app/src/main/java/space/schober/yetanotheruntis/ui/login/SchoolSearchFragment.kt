package space.schober.yetanotheruntis.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import space.schober.yetanotheruntis.R
import space.schober.yetanotheruntis.adapter.SchoolSearchAdapter
import space.schober.yetanotheruntis.untis.SchoolSearch
import space.schober.yetanotheruntis.untis.exceptions.TooManyResults


class SchoolSearchFragment : Fragment() {
    companion object {
        const val BACK_STACK_NAME = "SchoolSearchFragment"
        const val REQUEST_KEY = "SchoolSearchFragmentRequestKey"
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var manager: RecyclerView.LayoutManager
    private lateinit var myAdapter: RecyclerView.Adapter<*>

    private lateinit var txtSchoolName: TextView
    private lateinit var btnSchoolSearch: ImageButton

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_school_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtSchoolName = view.findViewById(R.id.txtSchoolSearch)
        btnSchoolSearch = view.findViewById(R.id.btnSchoolSearch)
        btnSchoolSearch.setOnClickListener {
            GlobalScope.launch { schoolSearch() }
        }

        manager = LinearLayoutManager(context)
        myAdapter = SchoolSearchAdapter(listOf(), this)

        recyclerView = view.findViewById<RecyclerView>(R.id.rvSchoolSearch).apply {
            layoutManager = manager
            adapter = myAdapter
        }
    }

    private suspend fun schoolSearch() {
        val query = txtSchoolName.text.toString()

        try {
            val schoolList = SchoolSearch.search(query)


            if (schoolList != null && schoolList.schools.isNotEmpty()) {
                myAdapter = SchoolSearchAdapter(schoolList.schools, this)
                requireActivity().runOnUiThread {
                    recyclerView =
                        requireActivity().findViewById<RecyclerView>(R.id.rvSchoolSearch).apply {
                            layoutManager = manager
                            adapter = myAdapter
                        }

                    val dividerItemDecoration = DividerItemDecoration(
                        recyclerView.context,
                        LinearLayout.VERTICAL
                    )
                    recyclerView.addItemDecoration(dividerItemDecoration)
                }
            } else {
                Snackbar.make(
                    requireView(),
                    getString(R.string.no_results_error),
                    Snackbar.LENGTH_LONG
                ).show()
            }
        } catch (e: TooManyResults) {
            Snackbar.make(
                requireView(),
                getString(R.string.too_many_results_error),
                Snackbar.LENGTH_LONG
            ).show()
        }
    }
}