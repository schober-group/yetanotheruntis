package space.schober.yetanotheruntis.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.core.os.bundleOf
import androidx.fragment.app.*
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import space.schober.yetanotheruntis.R
import space.schober.yetanotheruntis.keystore.MyKeystore
import space.schober.yetanotheruntis.ui.login.SelectKlassenFragment.Companion.EXTRA_KLASSEN
import space.schober.yetanotheruntis.ui.login.SelectKlassenFragment.Companion.EXTRA_PASSWORD
import space.schober.yetanotheruntis.ui.login.SelectKlassenFragment.Companion.EXTRA_USERNAME
import space.schober.yetanotheruntis.untis.Untis
import space.schober.yetanotheruntis.untis.data.Klasse
import space.schober.yetanotheruntis.untis.exceptions.AccountNotAdded
import space.schober.yetanotheruntis.untis.exceptions.InvalidLogin

class LoginFragment : Fragment() {
    companion object {
        const val REQUEST_KEY: String = "LoginFragmentRequestKey"
        const val BACK_STACK_NAME: String = "LoginFragmentBackStackName"

        const val EXTRA_SCHOOL_NAME = "SCHOOL_NAME"
        const val EXTRA_SCHOOL_URL = "SCHOOL_URL"
    }

    private lateinit var txtUsername: EditText
    private lateinit var txtPassword: EditText
    private lateinit var animationLogin: LottieAnimationView
    private lateinit var loginBtn: Button

    private lateinit var schoolName: String
    private lateinit var schoolURL: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setFragmentResultListener(SchoolSearchFragment.REQUEST_KEY) { _, bundle ->
            schoolName = bundle.getString(EXTRA_SCHOOL_NAME)!!
            schoolURL = bundle.getString(EXTRA_SCHOOL_URL)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        txtUsername = view.findViewById(R.id.txtUsername)
        txtPassword = view.findViewById(R.id.txtPassword)
        animationLogin = view.findViewById(R.id.animationLogin)
        loginBtn = view.findViewById(R.id.btnLogin)
        loginBtn.setOnClickListener {
            login()
        }
    }

    private fun login() {
        val username = txtUsername.text.toString()
        val password = txtPassword.text.toString()
        val untis = Untis(
            schoolURL,
            schoolName,
            username,
            password
        )

        requireActivity().runOnUiThread {
            animationLogin.visibility = View.VISIBLE
        }

        GlobalScope.launch {
            try {
                untis.validateLogin()

                val klassen = untis.getKlassen().also {
                    it?.forEach { klasse ->
                        if (klasse.id == untis.klasseID) klasse.selected = true
                    }
                }

                val bundle = bundleOf(
                    EXTRA_KLASSEN to klassen as ArrayList<Klasse>,
                    EXTRA_USERNAME to untis.username,
                    EXTRA_PASSWORD to MyKeystore.encrypt(requireContext(), password),
                    EXTRA_SCHOOL_NAME to schoolName,
                    EXTRA_SCHOOL_URL to schoolURL
                )

                setFragmentResult(REQUEST_KEY, bundle)
                parentFragmentManager.commit {
                    setCustomAnimations(
                        R.anim.slide_in,
                        R.anim.fade_out,
                        R.anim.fade_in,
                        R.anim.slide_out
                    )
                    replace<SelectKlassenFragment>(R.id.fragmentLogin)
                    setReorderingAllowed(true)
                    addToBackStack(BACK_STACK_NAME)
                }
            } catch (e: InvalidLogin) {
                errorMsg(loginBtn, getString(R.string.invalid_login))
            } catch (e: AccountNotAdded) {
                errorMsg(loginBtn, getString(R.string.account_not_added))
            } finally {
                requireActivity().runOnUiThread {
                    animationLogin.visibility = View.GONE
                }
            }
        }
    }


    private fun errorMsg(view: View, msg: String) {
        requireActivity().runOnUiThread {
            clearAll()
            Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show()
        }
    }

    private fun clearAll() {
        txtUsername.text.clear()
        txtPassword.text.clear()
    }
}
