package space.schober.yetanotheruntis.ui.login

import android.Manifest
import android.accounts.Account
import android.accounts.AccountManager
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.ContentResolver
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import permissions.dispatcher.*
import space.schober.yetanotheruntis.R
import space.schober.yetanotheruntis.adapter.KlassenAdapter
import space.schober.yetanotheruntis.permissions.PermissionManager
import space.schober.yetanotheruntis.untis.data.Klasse
import space.schober.yetanotheruntis.untis.exceptions.AccountNotAdded
import space.schober.yetanotheruntis.untis.helper.UntisKey
import space.schober.yetanotheruntis.untis.helper.UntisParser
import timber.log.Timber
import java.util.*


@RuntimePermissions
class SelectKlassenFragment : Fragment() {
    companion object {
        const val EXTRA_KLASSEN = "EXTRA_KLASSEN"
        const val EXTRA_USERNAME = "EXTRA_USERNAME"
        const val EXTRA_PASSWORD = "EXTRA_PASSWORD"

        const val BUNDLE_SELECTED_KLASSEN = "BUNDLE_SEL_KLASSEN"

        private const val SECONDS_PER_MINUTE = 60L
        private const val SYNC_INTERVAL_IN_MINUTES = 60L
        const val SYNC_INTERVAL = SYNC_INTERVAL_IN_MINUTES * SECONDS_PER_MINUTE
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var manager: RecyclerView.LayoutManager
    private lateinit var myAdapter: KlassenAdapter
    private lateinit var loadingAnimation: LottieAnimationView
    private lateinit var finishAnimation: LottieAnimationView
    private lateinit var addClass: FloatingActionButton

    private var klassen: ArrayList<Klasse> = arrayListOf()
    private lateinit var username: String
    private lateinit var password: String
    private lateinit var school: String
    private lateinit var url: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setFragmentResultListener(LoginFragment.REQUEST_KEY) { _, bundle ->
            klassen = bundle.getParcelableArrayList(EXTRA_KLASSEN)!!
            username = bundle.getString(EXTRA_USERNAME)!!
            password = bundle.getString(EXTRA_PASSWORD)!!
            school = bundle.getString(LoginFragment.EXTRA_SCHOOL_NAME)!!
            url = bundle.getString(LoginFragment.EXTRA_SCHOOL_URL)!!

            setupRecyclerView()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_select_klassen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        manager = LinearLayoutManager(context)
        loadingAnimation = view.findViewById(R.id.animationLoadingSelectKlassen)
        finishAnimation = view.findViewById(R.id.animationLoadingSelectKlassenFinish)
        addClass = view.findViewById(R.id.fabAddClass)
        addClass.setOnClickListener {
            showRationaleForAutoStart()
        }
        recyclerView = view.findViewById(R.id.rvKlassen)
        setupRecyclerView()
    }


    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        val bundleSelectedKlassen: List<Klasse>? = savedInstanceState?.getParcelableArrayList(
            BUNDLE_SELECTED_KLASSEN
        )
        if (bundleSelectedKlassen != null) {
            myAdapter.dataSet = bundleSelectedKlassen
        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelableArrayList(
            BUNDLE_SELECTED_KLASSEN,
            myAdapter.dataSet as ArrayList<out Parcelable>
        )

        super.onSaveInstanceState(outState)
    }

    private fun setupRecyclerView() {
        myAdapter = KlassenAdapter(klassen)
        recyclerView.apply {
            layoutManager = manager
            adapter = myAdapter
        }
    }

    @NeedsPermission(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR)
    fun addAccount() {
        startAnimation(loadingAnimation)
        val selectedClasses: ArrayList<Klasse> = arrayListOf()
        klassen.forEach {
            if (it.selected)
                selectedClasses.add(it)
        }

        addAccount(selectedClasses)
        startAnimation(finishAnimation)

        val animator = ValueAnimator.ofFloat(0f, 1f)
        animator.addUpdateListener {
            finishAnimation.progress = it.animatedValue as Float
        }

        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                Thread.sleep(2500)
                activity?.finishAndRemoveTask()
            }
        })

        requireActivity().runOnUiThread { animator.start() }
    }

    private fun startAnimation(lottieAnimationView: LottieAnimationView) {
        requireActivity().runOnUiThread {
            lottieAnimationView.visibility = View.VISIBLE
            lottieAnimationView.playAnimation()
        }
    }

    private fun addAccount(klassen: ArrayList<Klasse>) {
        try {
            createSyncAccount(klassen)
        } catch (error: AccountNotAdded) {
            //errorMsg("${klasse.name} ${getString(R.string.class_already_added)}")
            //klasse.selected = false
            requireActivity().runOnUiThread {
                myAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun createSyncAccount(syncedKlassen: ArrayList<Klasse>): Account {
        val newAccount = Account(school, this.getString(R.string.account_type))
        val authority = this.getString(R.string.content_authority)
        val syncedKlassenArray = syncedKlassen.toTypedArray()

        val bundle = Bundle(6)
        bundle.putString(UntisKey.SCHOOL.toString(), school)
        bundle.putString(UntisKey.URL.toString(), url)
        bundle.putString(UntisKey.USERNAME.toString(), username)
        bundle.putString(UntisKey.DATE_RANGE.toString(), "14")
        // bundle.putParcelableArray(UntisKey.SYNCED_CLASSES.toString(), syncedKlassenArray) <- this is not possible T.T
        // as only strings are allowed; see https://developer.android.com/reference/android/accounts/AccountManager#setUserData(android.accounts.Account,%20java.lang.String,%20java.lang.String)
        bundle.putString(
            UntisKey.SYNCED_CLASSES.toString(),
            UntisParser.classesToString(syncedKlassenArray)
        )

        val accountManager = AccountManager.get(context)

        return if (accountManager.addAccountExplicitly(newAccount, password, bundle)) {
            Timber.i("Added Account [${newAccount.name}] with following classes ${syncedKlassen.joinToString { it.name }}!")
            ContentResolver.setSyncAutomatically(newAccount, authority, true)
            ContentResolver.addPeriodicSync(
                newAccount,
                authority,
                Bundle.EMPTY,
                SYNC_INTERVAL
            )
            ContentResolver.requestSync(newAccount, authority, Bundle.EMPTY)
            newAccount
        } else {
            Timber.e("Account NOT added")
            throw AccountNotAdded()
        }
    }

    private fun showRationaleForAutoStart() {
        val mwAS = PermissionManager.isMwAS(Build.MANUFACTURER)

        if (mwAS != null)
            AlertDialog.Builder(requireContext())
                .setPositiveButton(R.string.button_allow) { _, _ ->
                    PermissionManager.showAutoStartup(
                        requireContext(),
                        mwAS
                    )

                    addAccountWithPermissionCheck()
                }
                .setNegativeButton(R.string.button_deny) { _, _ -> return@setNegativeButton }
                .setCancelable(false)
                .setMessage(R.string.autostart_required)
                .show()
        else
            addAccountWithPermissionCheck()
    }


    @OnShowRationale(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR)
    fun showRationaleForCalendar(request: PermissionRequest) {
        showRationaleDialog(R.string.calendar_denied, request)
    }

    private fun showRationaleDialog(@StringRes messageResId: Int, request: PermissionRequest) {
        AlertDialog.Builder(requireContext())
            .setPositiveButton(R.string.button_allow) { _, _ -> request.proceed() }
            .setNegativeButton(R.string.button_deny) { _, _ -> request.cancel() }
            .setCancelable(false)
            .setMessage(messageResId)
            .show()
    }

    @OnPermissionDenied(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR)
    fun onCalendarDenied() {
        Toast.makeText(context, R.string.permission_calendar_denied, Toast.LENGTH_SHORT).show()
    }

    @OnNeverAskAgain(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR)
    fun onNeverAskAgainDialogCalendar() {
        onNeverAskAgainDialog(R.string.calendar_denied)
    }


    private fun onNeverAskAgainDialog(@StringRes messageResId: Int) {
        AlertDialog.Builder(requireContext())
            .setPositiveButton(R.string.open_app_settings) { _, _ ->
                PermissionManager.showAppSettings(
                    requireContext()
                )
            }
            .setNegativeButton(R.string.button_deny) { _, _ -> return@setNegativeButton }
            .setCancelable(false)
            .setMessage(messageResId)
            .show()
    }


    private fun errorMsg(msg: String) {
        requireActivity().runOnUiThread {
            Snackbar.make(this.recyclerView, msg, Snackbar.LENGTH_LONG).show()
        }
    }
}