package space.schober.yetanotheruntis.ui.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import space.schober.yetanotheruntis.R

class AboutFragment : Fragment() {
    companion object {
        const val BACK_STACK_NAME = "AboutFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_about, container, false)
        root.findViewById<Button>(R.id.showLibrariesBtn).setOnClickListener {
            parentFragmentManager.commit {
                setCustomAnimations(
                    R.anim.slide_in,
                    R.anim.fade_out,
                    R.anim.fade_in,
                    R.anim.slide_out
                )
                replace<LibrariesFragment>(R.id.nav_host_fragment)
                setReorderingAllowed(true)
                addToBackStack(BACK_STACK_NAME)
            }
        }
        return root
    }
}