package space.schober.yetanotheruntis.ui.accounts

import android.accounts.AccountManager
import android.content.ContentResolver
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import space.schober.yetanotheruntis.R
import space.schober.yetanotheruntis.activities.LoginActivity
import space.schober.yetanotheruntis.adapter.AccountAdapter
import space.schober.yetanotheruntis.adapter.poko.ExtendedAccount
import space.schober.yetanotheruntis.datasync.AccountUtil
import space.schober.yetanotheruntis.untis.helper.UntisKey
import space.schober.yetanotheruntis.untis.helper.UntisParser
import timber.log.Timber
import java.util.*
import kotlin.collections.HashMap

class AccountsFragment : Fragment() {
    companion object {
        private const val BUNDLE_ACCOUNTS = "BUNDLE_ACCOUNTS"
        private const val CURRENTLY_SELECTED = "CURRENTLY_SELECTED"
        private const val EDIT_MODE = "EDIT_MODE"
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var manager: RecyclerView.LayoutManager
    private var myAdapter: AccountAdapter? = null

    private lateinit var fabAddAccount: FloatingActionButton
    private lateinit var fabRemoveAccount: FloatingActionButton

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_accounts, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setRecyclerView()

        fabAddAccount = view.findViewById(R.id.fabAddAccount)
        fabRemoveAccount = view.findViewById(R.id.fabRemoveAccount)
        fabAddAccount.setOnClickListener {
            val intent = Intent(this.context, LoginActivity::class.java)
            startActivity(intent)
        }
        fabRemoveAccount.setOnClickListener {
            val accounts = (recyclerView.adapter as AccountAdapter).data

            val accountManager = AccountManager.get(context)
            accounts.forEach { hme ->
                hme.value.forEach { account ->
                    if (account.selected)
                        accountManager.removeAccountExplicitly(account.getAccount(requireContext()))
                    else
                        ContentResolver.requestSync(
                            account.getAccount(requireContext()),
                            getString(R.string.content_authority),
                            Bundle.EMPTY
                        )
                }
            }

            setRecyclerView()
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (myAdapter?.editMode == true) {
                    myAdapter!!.deselectAll()
                } else {
                    requireActivity().finish()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    private fun setRecyclerView(view: View = requireView()) {
        manager = LinearLayoutManager(context)
        myAdapter =
            AccountAdapter(
                prepareData(),
                view
            )

        myAdapter!!.currentlySelected = 0

        recyclerView = view.findViewById<RecyclerView>(R.id.rvAccounts).apply {
            layoutManager = manager
            adapter = myAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        val temp = prepareData()

        if (recyclerView.adapter?.itemCount ?: 0 != temp.count()) {
            setRecyclerView()
        }
    }


    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        try {
            Timber.v("Loading savedInstanceState...")
            myAdapter!!.data =
                savedInstanceState!!.getSerializable(BUNDLE_ACCOUNTS) as HashMap<String, SortedSet<ExtendedAccount>>
            myAdapter!!.currentlySelected = savedInstanceState.getInt(CURRENTLY_SELECTED)
            myAdapter!!.editMode = savedInstanceState.getBoolean(EDIT_MODE)
        } catch (exception: ClassCastException) {
            Timber.e("Could not load selected accounts!")
        } catch (exception: NullPointerException) {
            Timber.v("Nothing to load")
        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        //FIXME
        if (myAdapter != null) {
            outState.putSerializable(BUNDLE_ACCOUNTS, myAdapter!!.data)
            outState.putInt(CURRENTLY_SELECTED, myAdapter!!.currentlySelected)
            outState.putBoolean(EDIT_MODE, myAdapter!!.editMode)
        }
        super.onSaveInstanceState(outState)
    }


    private fun prepareData(): HashMap<String, SortedSet<ExtendedAccount>> {
        val map = HashMap<String, SortedSet<ExtendedAccount>>()

        AccountManager.get(context).getAccountsByType(getString(R.string.account_type))
            .forEach { account ->
                val school = AccountUtil.getData(account, UntisKey.SCHOOL, requireContext())
                val syncedKlassenArray = UntisParser.stringToClasses(
                    AccountUtil.getData(
                        account,
                        UntisKey.SYNCED_CLASSES,
                        requireContext()
                    )!!
                )

                if (map[school] == null) { // If school does not exist -> add it
                    map[school!!] = sortedSetOf(
                        ExtendedAccount.Comparator,
                        ExtendedAccount(account)
                    )
                }

                syncedKlassenArray!!.forEach { klasse ->
                    map[school]!!.add(ExtendedAccount(account, klasse)) // Add all classes to Map
                }
            }

        return map
    }
}