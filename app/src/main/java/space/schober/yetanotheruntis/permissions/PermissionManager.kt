@file:Suppress("unused")

package space.schober.yetanotheruntis.permissions

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import space.schober.yetanotheruntis.BuildConfig
import timber.log.Timber

class PermissionManager {
    companion object {
        private const val TAG = "PermissionsManager"

        // The indices for the projection array above.
        private const val PROJECTION_ID_INDEX: Int = 0
        private const val PROJECTION_ACCOUNT_NAME_INDEX: Int = 1
        private const val PROJECTION_DISPLAY_NAME_INDEX: Int = 2
        private const val PROJECTION_OWNER_ACCOUNT_INDEX: Int = 3


        fun declaredInManifest(packageManager: PackageManager, permission: String): Boolean {
            val info = packageManager.getPackageInfo(
                BuildConfig.APPLICATION_ID,
                PackageManager.GET_PERMISSIONS
            )
            return info.requestedPermissions.contains(permission)
        }

        private fun getIntentAppSettings(context: Context): Intent {
            return Intent(
                android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
            )
        }

        fun showAppSettings(context: Context) {
            val intent = getIntentAppSettings(context)

            if (intent.resolveActivity(context.packageManager) != null)
                context.startActivity(intent)
            else
                Timber.w("App settings Intent not resolvable")

        }

        @Suppress("SpellCheckingInspection")
        enum class ManufacturersWithAutoStart(
            val manufacturer: String,
            val componentName: ComponentName
        ) {
            XIAOMI(
                "xiaomi", ComponentName(
                    "com.miui.securitycenter",
                    "com.miui.permcenter.autostart.AutoStartManagementActivity"
                )
            ),
            OPPO(
                "oppo", ComponentName(
                    "com.coloros.safecenter",
                    "com.coloros.safecenter.permission.startup.StartupAppListActivity"
                )
            ),
            VIVO(
                "vivo", ComponentName(
                    "com.vivo.permissionmanager",
                    "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"
                )
            ),
            LETV(
                "letv", ComponentName(
                    "com.letv.android.letvsafe",
                    "com.letv.android.letvsafe.AutobootManageActivity"
                )
            ),
            HONOR(
                "honor", ComponentName(
                    "com.huawei.systemmanager",
                    "com.huawei.systemmanager.optimize.process.ProtectActivity"
                )
            );

        }

        fun isMwAS(manufacturer: String): ManufacturersWithAutoStart? {
            return ManufacturersWithAutoStart.values()
                .find { it.manufacturer.equals(manufacturer, ignoreCase = true) }
        }

        fun showAutoStartup(context: Context, mwAS: ManufacturersWithAutoStart) {
            try {
                val intent = Intent().apply { component = mwAS.componentName }

                val list = context.packageManager.queryIntentActivities(
                    intent,
                    PackageManager.MATCH_DEFAULT_ONLY
                )

                if (list.size > 0)
                    context.startActivity(intent)

            } catch (e: Exception) {
                Timber.e(e.toString())
            }
        }
    }
}
