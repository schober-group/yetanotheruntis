package space.schober.yetanotheruntis.datasync

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Context
import space.schober.yetanotheruntis.untis.helper.UntisKey

object AccountUtil {
    fun getData(account: Account, untisKey: UntisKey, context: Context): String? =
        AccountManager.get(context).getUserData(account, untisKey.toString())
}