package space.schober.yetanotheruntis.datasync

import android.accounts.Account
import android.accounts.AccountManager
import android.content.*
import android.os.Bundle
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import space.schober.yetanotheruntis.BuildConfig
import space.schober.yetanotheruntis.calendar.CalendarUtil
import space.schober.yetanotheruntis.calendar.EventUtil
import space.schober.yetanotheruntis.keystore.MyKeystore
import space.schober.yetanotheruntis.untis.Untis
import space.schober.yetanotheruntis.untis.data.Timetable
import space.schober.yetanotheruntis.untis.data.TimetableParams
import space.schober.yetanotheruntis.untis.data.UntisTypes
import space.schober.yetanotheruntis.untis.exceptions.DateRangeNotWithinASingleSchoolYear
import space.schober.yetanotheruntis.untis.helper.UntisCalendar
import space.schober.yetanotheruntis.untis.helper.UntisKey
import space.schober.yetanotheruntis.untis.helper.UntisParser
import timber.log.Timber
import timber.log.Timber.DebugTree


class SyncAdapter @JvmOverloads constructor(
    context: Context,
    autoInitialize: Boolean,
    /**
     * Using a default argument along with @JvmOverloads
     * generates constructor for both method signatures to maintain compatibility
     * with Android 3.0 and later platform versions
     */
    allowParallelSyncs: Boolean = false,
    private val contentResolver: ContentResolver = context.contentResolver

) : AbstractThreadedSyncAdapter(context, autoInitialize, allowParallelSyncs) {
    init {
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
        Timber.v("Starting SyncAdapter...")
    }

    /**
     * Gets executed if account is synced (periodically and manual)
     */
    override fun onPerformSync(
        account: Account,
        extras: Bundle,
        authority: String,
        provider: ContentProviderClient,
        syncResult: SyncResult
    ) {
        Timber.v("onPerformSync...")
        //TODO("Cache sessionID")

        val encryptedPassword =
            AccountManager.get(context).getPassword(account).toString()
        val password = MyKeystore.decrypt(context, encryptedPassword)

        val school = getData(account, UntisKey.SCHOOL)
        val url = getData(account, UntisKey.URL)
        val username = getData(account, UntisKey.USERNAME)
        val dateRange = getData(account, UntisKey.DATE_RANGE).toIntOrNull() ?: 14
        val syncedKlassenArray =
            UntisParser.stringToClasses(getData(account, UntisKey.SYNCED_CLASSES))
        if (syncedKlassenArray != null)
            Timber.d("Syncing ${syncedKlassenArray.joinToString { it.name }} [$school]")
        else {
            Timber.w("No classes found to sync!")
            return
        }


        val untis = Untis(url, school, username, password)
        syncedKlassenArray.forEach { klasse ->
            val calID = CalendarUtil.manageCalendar(context, klasse.name, school)

            GlobalScope.launch {
                untis.validateLogin()
                var timetable: List<Timetable>? = null

                try {
                    timetable = getTimetable(untis, klasse.id.toString(), dateRange)
                } catch (error: DateRangeNotWithinASingleSchoolYear) {
                    timetable = reduceRange(dateRange, untis, klasse.id.toString())
                } finally {
                    if (timetable != null) {
                        timetable.forEach {
                            EventUtil.addToCalendar(
                                contentResolver,
                                it,
                                calID,
                                school
                            )
                        }
                    } else {
                        Timber.w("Cannot sync timetable")
                    }
                }
            }
        }
    }


    private fun getData(account: Account, untisKey: UntisKey): String =
        AccountUtil.getData(account, untisKey, context)!!


    /**
     * Returns the timetable for a specific class and date range
     */
    private suspend fun getTimetable(untis: Untis, klassenID: String, dateRange: Int) =
        untis.getTimetable(
            TimetableParams.Options(
                TimetableParams.Element(
                    id = klassenID,
                    type = UntisTypes.KLASSE.toLong()
                ),
                endDate = UntisCalendar.getDateInXTDays(dateRange)
            )
        )

    /**
     * Reduces date range until it fits
     *
     * @param originalRange The original rang, which is to broad
     * @param untis The untis object
     * @param klassenID The ID of the class
     */
    private suspend fun reduceRange(
        originalRange: Int,
        untis: Untis,
        klassenID: String
    ): List<Timetable>? {
        var timetable: List<Timetable>? = null
        var range = originalRange

        do {
            Timber.i("Syncing a too broad range [$range] days... Reducing range")
            // If range is to broad -> half it or subtract one day from it
            if (range > 14) range /= 2 else range--

            try {
                timetable = getTimetable(untis, klassenID, range)
            } catch (error: DateRangeNotWithinASingleSchoolYear) {
                Timber.d("Trying again")
            }
        } while (timetable == null && range >= 1)

        return timetable
    }
}
