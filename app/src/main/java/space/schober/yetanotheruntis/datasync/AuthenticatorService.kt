package space.schober.yetanotheruntis.datasync

import android.app.Service
import android.content.Intent
import android.os.IBinder
import timber.log.Timber

class AuthenticatorService : Service() {
    // Instance field that stores the authenticator object
    private lateinit var mAuthenticator: Authenticator

    override fun onCreate() {
        Timber.d("Creating AuthenticatorService...")
        // Create a new authenticator object
        mAuthenticator = Authenticator(this)
    }

    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    override fun onBind(intent: Intent?): IBinder = mAuthenticator.iBinder
}
