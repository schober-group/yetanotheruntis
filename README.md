# YAU

YAU, short for Yet Another Untis, is an app, that lets you sync your Untis schedule with your android calendar. This way you have both your private calendar and your lessons next to each other and it is easier to manager events.

## Installation

You can download the latest [Releases]() for your Platform from the release section or build it yourself.

### Build Instructions

#### Android Studio

- Clone the Project from GitLab
- Open it in Android Studio
- Build -> Bundle(s) / APK(s) -> Build APK(s)

#### Command Line

```bash
./gradlew assembleDebug
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.